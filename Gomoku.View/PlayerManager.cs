﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomoku.View
{
    public class PlayerManager : IPlayerManager
    {
        public IPlayer Player1 { get; private set; }

        public IPlayer Player2 { get; private set; }

        public PlayerManager()
        {
            Player1 = new Player();
            Player2 = new Player();
        }

        public Player LoginPlayer(Pages.GameView fetchData)
        {
            Player player = Player1 as Player;
            if (player.FetchData == null)
            {
                player.FetchData = fetchData;
                return player;
            }
            player = Player2 as Player;
            if (player.FetchData == null)
            {
                player.FetchData = fetchData;
                return player;
            }

            return null;
        }
        public void LogoutPlayer(Player player)
        {
            player.FetchData = null;
        }

    }
}
