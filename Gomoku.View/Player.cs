﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomoku.View
{
    public delegate void PlayerOnTurn();
    public class Player : IPlayer
    {
        public Pages.GameView FetchData = null;
        PlayerStates isPlayersTurn = PlayerStates.Inactive;
        public PlayerStates PlayerState
        { 
            get => isPlayersTurn; 
            set
            {
                isPlayersTurn = value;
                FetchData?.UpdateView();
            }
        }
    }
}
