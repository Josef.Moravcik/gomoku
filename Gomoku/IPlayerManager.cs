﻿namespace Gomoku
{
    public interface IPlayerManager
    {
        public IPlayer Player1 { get; }
        public IPlayer Player2 { get; }

    }
}