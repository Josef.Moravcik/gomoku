﻿using System;

namespace Gomoku
{
    public delegate void FieldOccupied(Field field);
    public class Field
    {
        public FieldOccupied FieldOccupied;
        public readonly int Col, Row;

        public Field(int row, int col)
        {
            Col = col;
            Row = row;
        }

        IPlayer player = null;
        public IPlayer Player
        {
            get => player;
            set
            {
                if (player != null) return;
                player = value;
                FieldOccupied?.Invoke(this);
            }
        }


    }
}
