﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gomoku
{
    public class Game
    {
        public const int MaxCol = 30;
        public const int MaxRow = 30;
        public Field[,] fields { get; }

        public IPlayerManager PlayerManager { get; }

        public Game(IPlayerManager playerManager)
        {
            fields = new Field[MaxRow, MaxCol];
            for (int i = 0; i < MaxRow; i++)
            {
                for (int j = 0; j < MaxCol; j++)
                {
                    fields[i, j] = new Field(i, j);
                    fields[i, j].FieldOccupied += Field_FieldOccupied;
                }
            }
            PlayerManager = playerManager;
            PlayerManager.Player1.PlayerState = PlayerStates.Playing;
        }

        public void Field_FieldOccupied(Field field)
        {
            if (playerWon(field))
            {
                if (field.Player == PlayerManager.Player1)
                {
                    PlayerManager.Player1.PlayerState = PlayerStates.Won;
                    PlayerManager.Player2.PlayerState = PlayerStates.Lost;
                }
                else
                {
                    PlayerManager.Player1.PlayerState = PlayerStates.Lost;
                    PlayerManager.Player2.PlayerState = PlayerStates.Won;
                }
                return;
            }

            if (field.Player == PlayerManager.Player1)
            {
                PlayerManager.Player1.PlayerState = PlayerStates.Inactive;
                PlayerManager.Player2.PlayerState = PlayerStates.Playing;
            }
            else
            {
                PlayerManager.Player1.PlayerState = PlayerStates.Playing;
                PlayerManager.Player2.PlayerState = PlayerStates.Inactive;
            }
        }

        private bool playerWon(Field field)
        {
            return checkHorizontal(field) || checkVertical(field) || checkDiagonalLeft(field) || checkDiagonalRight(field);
        }

        private bool checkDiagonalRight(Field field)
        {
            int col = field.Col;
            int row = field.Row;
            int counter = 0;
            for (int i = 1; col + i < MaxCol && row - i >= 0; i++)
            {
                if (field.Player == fields[row - i, col + i].Player)
                    counter++;
                else break;
            }
            for (int i = 1; col - i >= 0 && row + i < MaxRow; i++)
            {
                if (field.Player == fields[row + i, col - i].Player)
                    counter++;
                else break;
            }

            if (counter == 4)
                return true;

            return false;
        }

        private bool checkDiagonalLeft(Field field)
        {
            int col = field.Col;
            int row = field.Row;
            int counter = 0;
            for (int i = 1; col + i < MaxCol && row + i < MaxRow; i++)
            {
                if (field.Player == fields[row + i, col + i].Player)
                    counter++;
                else break;
            }
            for (int i = 1; col - i >= 0 && row - i >= 0; i++)
            {
                if (field.Player == fields[row - i, col - i].Player)
                    counter++;
                else break;
            }
            if (counter == 4)
                return true;

            return false;
        }

        private bool checkVertical(Field field)
        {
            int col = field.Col;
            int row = field.Row;
            int counter = 0;
            for (int i = 1; row + i < MaxRow; 
                i++)
            {
                if (field.Player == fields[row + i, col].Player)
                    counter++;
                else break;
            }
            for (int i = 1; row - i >= 0; i++)
            {
                if (field.Player == fields[row - i, col].Player)
                    counter++;
                else break;
            }
            if (counter == 4)
                return true;

            return false;
        }

        private bool checkHorizontal(Field field)
        {
            int col = field.Col;
            int row = field.Row;
            int counter = 0;
            for (int i = 1; col + i < MaxCol; i++)
            {
                if (field.Player == fields[row, col + i].Player)
                    counter++;
                else break;
            }
            for (int i = 1; col - i >= 0; i++)
            {
                if (field.Player == fields[row, col - i].Player)
                    counter++;
                else break;
            }
            if (counter == 4)
                return true;

            return false;
        }
    }
}
